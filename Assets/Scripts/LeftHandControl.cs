﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftHandControl : MonoBehaviour
{
    public float speed;

    public float speedAngle;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            transform.Translate(Vector3.forward*Time.deltaTime*speed);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(Vector3.left*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Translate(Vector3.up*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.down*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(Time.deltaTime*speedAngle ,0,0);
        }
        if (Input.GetKey(KeyCode.X))
        {
            transform.Rotate(-Time.deltaTime*speedAngle ,0,0);
        }
        if (Input.GetKey(KeyCode.C))
        {
            transform.Rotate(0 ,Time.deltaTime*speedAngle,0);
        }
        if (Input.GetKey(KeyCode.V))
        {
            transform.Rotate(0 ,-Time.deltaTime*speedAngle,0);
        }
        if (Input.GetKey(KeyCode.B))
        {
            transform.Rotate(0 ,0,Time.deltaTime*speedAngle);
        }
        if (Input.GetKey(KeyCode.N))
        {
            transform.Rotate(0 ,0,-Time.deltaTime*speedAngle);
        }
    }
}
