﻿using UnityEngine;


public class RightHandControl : MonoBehaviour
{
    public float speed;
    public float speedArc;
    public float forceAdded;
    public float speedAngle;
    [SerializeField]
    private float _radius;
    private float _countdownToInstantiateArrow = 3f;
    private float _countdownToSetArrowPos = .1f;
    
    public bool canMove, canRotate;
    
    private bool _arrowThrow = false;
    [Header("Transforms")]
    public Transform pointMax;
    public Transform catchpoint;
    public Transform stockFleche;
    public Transform arc;
    public GameObject arrow, cordeP1, cordeP2, currentArrow;

    private Quaternion _cordePos1, _cordePos2;
    
    private Vector3 _arrowPosStart;
    
    private ObjectPooler objectPooler;

   
    float lastRandomnb = -1;
    private bool wind;
    private float randomwind;
    
    private float randomnb1 = 0.5f;
    private float randomnb2 = 2f;
    private void Start()
    {
        _cordePos1 = cordeP1.transform.rotation;
        _cordePos2 = cordeP2.transform.rotation;
        _arrowPosStart = currentArrow.transform.position;
        objectPooler = ObjectPooler.Instance;
        ReceiveData();
    }

    float RandomWindForce(float min, float max)
    {    //On prend une valeur random d'un float
         float random = Random.Range(min, max);
        while (random == lastRandomnb)
        {
            random = Random.Range(min, max);
            
        }

        lastRandomnb = random;
        return random;

    }
    // Update is called once per frame
    void Update()         // Access to the player to move
    {
        if (Vector3.Distance(gameObject.transform.position, catchpoint.transform.position ) <= _radius && Input.GetKey(KeyCode.K) && !canMove)
        {
            gameObject.transform.SetParent(catchpoint.gameObject.transform);
            gameObject.transform.position = catchpoint.gameObject.transform.position;
            canMove = true;
        }

        if (Input.GetKeyDown(KeyCode.V) && !wind)
        {
            wind = true;
            Debug.Log("wind active");
        }
        else if (Input.GetKeyDown(KeyCode.V) && wind)
        {
            wind = false;
            Debug.Log("wind desactivate");
        }
            
        if (!canMove)
        {
            Movement();
        }
        
        if (canMove)
        {
            if (Input.GetKey(KeyCode.Keypad5)) //Rotate the rope to the local position of the hand
            {
                if (canRotate)
                {
                    transform.Translate(Vector3.back*Time.deltaTime*speed);
                    cordeP1.transform.Rotate(0,0, gameObject.transform.localPosition.x * speedArc);
                    cordeP2.transform.Rotate(0,0, -gameObject.transform.localPosition.x * speedArc);
                    Debug.Log(currentArrow.transform.localPosition);
                    currentArrow.transform.localPosition = gameObject.transform.localPosition;
                }
            }
            
            //this is for the movement of the arrow
            if (gameObject.transform.position.z <= pointMax.transform.position.z)
            {
                gameObject.transform.position = pointMax.transform.position;
                canRotate = false;
                if (Input.GetKeyUp(KeyCode.K))
                {
                    if (wind)
                    {
                        randomwind = RandomWindForce(randomnb1, randomnb2); 
                    }
                    
                    cordeP1.transform.rotation = _cordePos1;
                    cordeP2.transform.rotation = _cordePos2;
                    currentArrow.GetComponent<Rigidbody>().AddForce(new Vector3(0,0,forceAdded),ForceMode.Impulse);
                    currentArrow.GetComponent<Rigidbody>().AddForce(new Vector3(randomwind,0,0),ForceMode.Impulse);
                    currentArrow.GetComponent<Rigidbody>().useGravity = true;
                    Debug.Log(randomwind);
                    canMove = false;
                    canRotate = true;
                    _arrowThrow = true;
                    gameObject.transform.SetParent(null);
                }
            }
        }
        
        if (_arrowThrow)
        {
            _countdownToInstantiateArrow -= Time.deltaTime;
            _countdownToSetArrowPos -= Time.deltaTime;
            if (_countdownToSetArrowPos<=0 && currentArrow != null)
            {
                objectPooler.SpawnFromPool("ArrowPos",currentArrow.transform.position,Quaternion.identity);
                _countdownToSetArrowPos = .1f;
            }
            
            if (_countdownToInstantiateArrow <=0)
            {
                Destroy(currentArrow);
                currentArrow = Instantiate(arrow, stockFleche.transform.position, Quaternion.identity);
                currentArrow.GetComponent<Rigidbody>().useGravity = false;
                currentArrow.transform.SetParent(stockFleche);
                _countdownToInstantiateArrow = 3;
                _arrowThrow = false;
            }
        }
    }

    void ReceiveData()
    {
        Vector3 vO = arc.transform.forward * forceAdded;
        float alpha = arc.transform.localEulerAngles.x;
        float beta = arc.transform.localEulerAngles.y;
        //On calcule om'
        Vector3 omp = vO * Mathf.Cos(alpha);
        //On détermine ensuite vx grace a om'
        Vector3 vx = omp * Mathf.Cos(beta);
        //On peut ensuite calculer vz
        Vector3 vz = omp * Mathf.Cos(90 - beta);
        //On peut calculer vy en suivant le meme principe que pour vz
        Vector3 vy = omp * Mathf.Cos(90 - alpha);
        
        Debug.Log(omp);
        Debug.Log(vx);
        Debug.Log(vz);
        Debug.Log(vy);

    }
    void Movement()
    {
        if (Input.GetKey(KeyCode.Keypad8))
        {
            transform.Translate(Vector3.forward*Time.deltaTime*speed);
        }

        if (Input.GetKey(KeyCode.Keypad4))
        {
            transform.Translate(Vector3.left*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.Keypad6))
        {
            transform.Translate(Vector3.right*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.Keypad9))
        {
            transform.Translate(Vector3.up*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.Keypad7))
        {
            transform.Translate(Vector3.down*Time.deltaTime*speed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Time.deltaTime*speedAngle ,0,0);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(-Time.deltaTime*speedAngle ,0,0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0 ,Time.deltaTime*speedAngle,0);
        }
        if (Input.GetKey(KeyCode.Keypad1))
        {
            transform.Rotate(0 ,-Time.deltaTime*speedAngle,0);
        }
        if (Input.GetKey(KeyCode.Keypad2))
        {
            transform.Rotate(0 ,0,Time.deltaTime*speedAngle);
        }
        if (Input.GetKey(KeyCode.Keypad3))
        {
            transform.Rotate(0 ,0,-Time.deltaTime*speedAngle);
        }
    }

}
