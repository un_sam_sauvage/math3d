﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform pointMin, pointMax;

    public GameObject arrow;

    // Update is called once per frame
    void Update()
    {
        arrow.transform.position = pointMax.transform.position;
    }
}
