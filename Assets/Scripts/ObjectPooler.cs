﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionnary;

    #region Singleton
    public static ObjectPooler Instance;
    private void Awake()
    {
        Instance = this;
    }
#endregion
    private void Start()
    {
        poolDictionnary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject objectInstantiate = Instantiate(pool.prefab);
                objectInstantiate.SetActive(false);
                objectPool.Enqueue(objectInstantiate);
            }
            poolDictionnary.Add(pool.tag,objectPool);
        }
    }

    public void SpawnFromPool(string tag, Vector3 pos, Quaternion rotation)
    {

        GameObject objectToSpawn  = poolDictionnary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = pos;
        objectToSpawn.transform.rotation = rotation;

        if (objectToSpawn.GetComponent<IPooledObject>() != null)
        {
            objectToSpawn.GetComponent<IPooledObject>().OnObjectSpawn();
        }
        
        poolDictionnary[tag].Enqueue(objectToSpawn);
    }
}
