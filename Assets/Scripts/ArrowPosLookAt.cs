﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPosLookAt : MonoBehaviour,IPooledObject
{
    private float _countdownToLookAt = .15f;
    private bool _allowLookAt = false;
    public void OnObjectSpawn()
    {
        
        _allowLookAt = true;
    }

    public void Update()
    {
        if (_allowLookAt)
        {
            _countdownToLookAt -= Time.deltaTime;
            if (_countdownToLookAt <=0 && GameObject.FindGameObjectWithTag("Player").GetComponent<RightHandControl>().currentArrow != null )
            {
                transform.LookAt(GameObject.FindGameObjectWithTag("Player").GetComponent<RightHandControl>().currentArrow.transform);
                _allowLookAt = false;
            }
        }
    }
}
